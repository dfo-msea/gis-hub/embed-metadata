# embed-metadata

__Main author:__  Cole Fields    
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250-363-8060


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
  + [Modules](#modules)
  + [Virtual Env](#virtual-env)
  + [Parameters](#parameters)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Python script to embed GIS Hub metadata into a file geodatabase (and feature classes) or shapefile.
This is done by retrieving the GIS Hub metadata for a dataset, and mapping a subset of it into the dataset metadata. 


## Summary
The need for this tool arose when we realized some datasets had metadata that contradicted the GIS Hub metadata. The purpose of the
script is to have a dataset that has embedded metadata which is consistent (in terms of information) to that of the GIS Hub metadata.
Because the GIS Hub metadata is a superset of the NAP metadata profile, some of the fields don't map to an established metadata standard.
As a workaround, some fields are mapped into other fields in the embedded metadata (e.g. Methods from the GIS Hub is mapped to Supplemental Information).


## Status
Completed


## Contents
Single python script meant to be run from the command line interface.

## Methods
The script attempts to make a list (even a list of 1) of the spatial layers that metadata will be embedded into. It creates a CKAN session
using `RemoteCKAN` and the user's API key. Next, it requests the metadata for a dataset using the provided paramater of the last portion of the dataset's URL.
The metadata is returned as JSON which is converted to XML format. Next, elements are extracted from the XML and inserted into the spatial layer(s) iteratively
using `arcpy_metadata`.


## Requirements

### Modules

* `dicttoxml`
* `ckanapi`
* `arcpy_metadata`
* `xml.tree.ElementTree` <sup>1</sup>
* `json` <sup>1</sup>

<sup>1</sup> should be installed by default with Anaconda

### Virtual Env

Steps modified from here: https://sites.temple.edu/geodev/setting-up-a-python-development-environment-with-anaconda-and-arcpy/.
The `arcpy_metadata` module has a dependency on the 32-bit version of `arcpy`. It will not work with the 64-bit Background Geoprocessing 
version of python. Virtual environment needs to be activated before running script.

1. Start Anaconda Prompt (installed with 32-bit Anaconda)
2. Create a virtual environment to use with ArcPy and other required modules

`conda create --name arcpy-venv python=2.7`

This will create a directory (under your user) C:\Users\FieldsC\AppData\Local\Continuum\anaconda3\envs called arcpy-venv 
or whatever name was chosen.

3. Activate the new environment in the Anaconda Prompt

`conda activate arcpy-venv`

4. Using Windows Explorer, copy the **Desktop10.6.pth** file (likely in this directory: C:\Python27\ArcGIS10.6\Lib\site-packages) to your virtual 
environment directory (in C:\Users\FieldsC\AppData\Local\Continuum\anaconda2\envs\arcpy-venv) replacing FieldsC with your username

5. Use a text editor to create a new file with the following line (replacing 10.6 with your version): **C:\Python27\ArcGIS10.6\Lib\site-packages**

6. Save the file as: **C:\Users\FieldsC\AppData\Local\Continuum\anaconda2\envs\arcpy-venv\Lib\site-packages\arcgis-python-site-packages.pth**
or wherever your virtual env is (replace FieldsC with your username).

7. Navigate to C:\Users\FieldsC\AppData\Local\Continuum\anaconda2\envs\arcpy-venv and double-click python.exe to open
a python shell (interpreter). Type `import arcpy`. If it imports, then the steps above have worked. 

8. Run the following commands in the Anaconda Prompt to install modules:

`conda install -c conda-forge dicttoxml`

`conda install -c conda-forge ckanapi`

`pip install arcpy_metadata`

`conda deactivate`

### Parameters
1. user's private CKAN API key (located on user account page on gis-hub.ca)
2. the last part of the dataset's URL (i.e. sponge-reefs for the dataset https://www.gis-hub.ca/dataset/sponge-reefs)
3. path to spatial file to edit metadata (raster, directory with shapefiles or rasters, shapefile, or file geodatabase)


## Caveats
* It is recommended to create a copy of the data prior to running this in case of error(s).
* Embeds metadata from a single source - so it will be the same for each of the spatial layers in the list if there are more than 1.
* Has been tested on shapefile, feature classes, and TIFF files. Other formats have not been tested.
* Does not embed metadata directly to the gdb (file geodatabase), but works on the feature classes within.
* Not meant to be a comprehensive metadata source, but rather some essential information if user obtains a copy of the data
but does not have access to the GIS Hub metadata. 


## Uncertainty
NA


## Acknowledgements
NA


## References
* https://sites.temple.edu/geodev/setting-up-a-python-development-environment-with-anaconda-and-arcpy/
* https://github.com/ucd-cws/arcpy_metadata
* https://github.com/ckan/ckanapi