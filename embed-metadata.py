# coding=utf-8
# import modules
import dicttoxml
from ckanapi import RemoteCKAN
import arcpy_metadata as md
import arcpy
import xml.etree.ElementTree as ET
import json
import os

# url to CKAN Instance
CKAN_URL = "https://gis-hub.ca"

# user's private CKAN API key (located on user account page on gis-hub.ca)
api_key = raw_input("Enter your CKAN API key: ")

# the last part of the dataset's URL (i.e. sponge-reefs for the dataset https://www.gis-hub.ca/dataset/sponge-reefs)
data_url = raw_input("Enter the last part of the URL for the GIS Hub dataset: ")

# get path to spatial layer to edit metadata
path_to_data = raw_input("Enter the path to the data: ")

# if path is to a folder
if os.path.exists(path_to_data):
    if path_to_data.split('\\')[-1][-3:] == "shp":
        # set spatial_layers to single shapefile
        spatial_layers = path_to_data
    elif path_to_data.split('\\')[-1][-3:] == "gdb":
        # Set the workspace environment to local file geodatabase
        arcpy.env.workspace = path_to_data
        # make list of feature classes
        spatial_layers = arcpy.ListFeatureClasses()

    else:
        # Set the workspace environment to local file geodatabase
        arcpy.env.workspace = path_to_data
        os.chdir(path_to_data)
        # make list of rasters
        raster_names = arcpy.ListRasters()
        spatial_layers = []
        for name in raster_names:
            spatial_layers.append(os.path.join(arcpy.env.workspace, name))


# use RemoteCKAN to connect to gis-hub site, authorize with api key
print("Connecting to GIS Hub: ")
gis_hub = RemoteCKAN(CKAN_URL, apikey=api_key)
print("...done\n")

# request metadata for specific dataset
print("Requesting metadata for {}: ".format(data_url))
meta_json = gis_hub.action.package_show(id=data_url)
print("...done\n")

# convert json metadata to xml format
print("Converting JSON to XML string:")
meta_xml = dicttoxml.dicttoxml(meta_json)
print("...done\n")


def get_root(xml_string):
    """
    :param xml_string: path to input xml file
    :return: root_element(xml.etree.ElementTree.Element)
    """

    # parse XML from a string directly into an Element, which is the root element of the parsed tree
    print("Parsing XML string into a root element from XML tree: ")
    root = ET.fromstring(xml_string)
    print("...done\n")
    return root


def get_metadata(root_elem):
    """
    :param root_elem: the root tag from the element tree
    :return: meta_dict: the dictionary with metadata elements from gis-hub metadata
    """

    print("Extracting metadata from XML root element: ")

    # get 'notes', which is the summary on the gis hub, which will be used as description for esri
    description = root_elem.find('.//notes')

    # create dictionary for metadata and add key:value pairs from xml from description
    meta_dict = {'description': description.text}

    # get methods and update dictionary with methods (supplementary information)
    methods = root_elem.find('.//methods')
    meta_dict.update({'supplemental': methods.text})

    # get citation & update dictionary with citation (credits)
    credits = root_elem.find('.//citation')
    meta_dict.update({'credits': credits.text})

    # get tags as a list (GOC keywords) & update dictionary with list of keywords (tags)
    keywords = root_elem.find('.//keywords').text.split(',')
    meta_dict.update({'tags': keywords})

    # get data creator (point of contact)
    point_of_contact = json.loads(root_elem.find('.//data_creator').text)
    meta_dict.update({'point_of_contact': point_of_contact})

    print("...done\n")
    return meta_dict


def edit_metadata(sp_layer, metadata_dictionary):
    """
    :param sp_layer: spatial layer to start a metadata editing session
    :param metadata_dictionary: dictionary of metadata from gis-hub
    :return: metadata object
    """
    # create a MetadataEditor object in order to access/edit existing metadata
    print("Creating metadata edit session for: {}".format(sp_layer))
    metadata = md.MetadataEditor(sp_layer)
    print("...done\n")

    # update description
    print("Updating metadata description: ")
    metadata.abstract = metadata_dictionary['description']
    print("...done\n")

    # update tags
    print("Updating metadata tags: ")
    metadata.tags = metadata_dictionary['tags']
    print("...done\n")

    # update credits
    print("Updating metadata credits: ")
    metadata.credits = metadata_dictionary['credits']
    print("...done\n")

    # update supplemental information
    print("Updating metadata supplemental information: ")
    metadata.supplemental_information = metadata_dictionary['supplemental']
    print("...done\n")

    # update point of contact
    print("Updating metadata point of contact: ")
    metadata.point_of_contact.contact_name = metadata_dictionary['point_of_contact']['creator_name']
    metadata.point_of_contact.email = metadata_dictionary['point_of_contact']['creator_email']
    print("...done\n")

    # save() changes to embedded metadata in spatial layer and cleanup() temporary files
    metadata.finish()
    print("...done\n")

    return metadata


# get root element
xml_root = get_root(meta_xml)

# get relevant metadata
hub_meta = get_metadata(xml_root)

print(spatial_layers)

# edit metadata for spatial layer
for layer in spatial_layers:
    edit_metadata(layer, hub_meta)